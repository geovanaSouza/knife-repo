# Chef Server

This repository has a VagrantFile to create a Chef Server in your machine

## Pre-requisites

* Vagrant
* VirtualBox
* Vagrant Timezone Plugin (vagrant plugin install vagrant-timezone)

## How to use

Start the environment

```bash
vagrant up
```

Edit your hosts to add the Chef Server local URL

```bash
echo 192.168.33.199 learn-chef.automate | sudo tee -a /etc/hosts
```

Get the chef key from server. Use the password `vagrant`

```bash
scp -P 2222 vagrant@127.0.0.1:/home/vagrant/user1.pem ~/.chef/user1.pem
```

Update or create your credentials file to make possible to connect to Chef Server

On file `~/.chef/credentials` (Create if not exist and update it if exist)

```bash
[learn-chef]
client_name = "user1"
client_key = "~/.chef/user1.pem"
chef_server_url = "https://learn-chef.automate/organizations/chef_foundations"
```

Change the knife profile

```bash
knife config use learn-chef
```

Get the self-signed SSL Cert

```bash
knife client ssl
```

List all registered clients

```bash
knife client list
```

Reload / update the vagrant provisioner configuration:

```bash
vagrant reload
```

Suspend the VM if you'll use later

```bash
vagrant suspend
```

To return to work with Chef Infra Server

```bash
vagrant resume
```

## Clean the environment

```bash
vagrant destroy
```